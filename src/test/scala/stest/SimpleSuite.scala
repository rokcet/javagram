package stest

import javagram.exceptions.UserException
import org.scalatest.FunSuite

class SimpleSuite extends FunSuite with ServerSetup {

  test("Invalid user id"){
    val invalid = store.createUser().getId + 1
    assertThrows[UserException]{
      store.getRemoteUser(invalid)
    }
  }

  test("Invalid post id"){
    val user = store.createUser()
    val invalid = user.post("last post").getId + 1
    assertThrows[UserException]{
      user.comment(invalid, "shouldn't post")
    }
  }

}
