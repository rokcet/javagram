package stest

//import java.rmi.server.{RemoteObject, UnicastRemoteObject}

import java.rmi.server.UnicastRemoteObject

import javagram.server.{LocalStore, Store}
import org.scalatest.{BeforeAndAfter, Suite}

trait ServerSetup extends BeforeAndAfter { self: Suite =>

  val text: Array[String] = Array("f", "hello", "test", "server", "random", "and I am Iron man")
  val rand = new scala.util.Random

  var store: LocalStore = _
  //var server: LocalStore = _

  before {
    store = new LocalStore("TestStore")
    //store = RemoteObject.toStub(server).asInstanceOf[Store]
  }

  after {
  }
}
