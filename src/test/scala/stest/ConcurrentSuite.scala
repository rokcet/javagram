package stest

import java.util.concurrent.CountDownLatch

import javagram.data.{RemotePost, RemoteUser}
import javagram.server.LocalStore
import org.scalatest.FunSuite

import scala.collection.mutable.ListBuffer
import scala.util.{Random, Sorting}

class ConcurrentSuite extends FunSuite with ServerSetup{

  for ((t, op) <- List((5, 1000), (5, 10000), (8, 1000), (8, 10000), (16, 1000), (16, 10000), (100, 1000), (1000, 50))){

    test(s"Likes/comments on 1 post (parallel) ($t threads, $op operations)") {
      val user = store.createUser()
      //val post = user.post(text(rand.nextInt(text.length)))
      val post = user.post("The post")

      //val semaphore: Semaphore = new Semaphore()
      val latch: CountDownLatch = new CountDownLatch(t)
      val startLatch: CountDownLatch = new CountDownLatch(1)

      for (_ <- 1 to t) {
        val testUser = store.createUser()
        val comment = "Press F to pay respects"//text(rand.nextInt(text.length))

        val thread = new Thread {
          override def run(): Unit = {
            startLatch.await()
            for (j <- 1 to op) {
              testUser.like(post)
              testUser.comment(post, comment + j)
            }

            //count up semaphore
            latch.countDown()
          }
        }
        //save thread
        thread.start()
      }

      startLatch.countDown()

      //wait on semaphore
      latch.await()

      //assert
      assert(post.getLikes == t)
      val comments = post.getComments
      assert(comments.length == t * op)

    }
  }

  for ((t, op) <- List((5, 1000), (5, 10000), (8, 1000), (8, 10000), (16, 1000), (16, 10000), (100, 1000), (1000, 50))){
    test(s"Unique comments on 1 post (parallel) ($t threads, $op comments)") {

      val user = store.createUser()
      //val post = user.post(text(rand.nextInt(text.length)))
      val post = user.post("The post")

      //val semaphore: Semaphore = new Semaphore()
      val latch: CountDownLatch = new CountDownLatch(t)
      val startLatch: CountDownLatch = new CountDownLatch(1)

      for (i <- 1 to t) {
        val testUser = store.createUser()
        val comment = s"i am # $i, " //text(rand.nextInt(text.length))

        val thread = new Thread {
          override def run(): Unit = {
            startLatch.await()
            for (j <- 1 to op)
              testUser.comment(post, comment + j)

            //count up semaphore
            latch.countDown()
          }
        }
        //save thread
        thread.start()
      }

      startLatch.countDown()
      //wait on semaphore
      latch.await()

      val comments = post.getComments

      val texts = comments.map(post => post.getMessage)
      Sorting.quickSort(texts)

      for ((a, i) <- texts.zipWithIndex) {
        if(i != texts.length-1)
          assert(a != texts(i+1), "Comments should be unique here")
      }

    }
  }

  for ((t, op) <- List((5, 1000), (5, 10000), (8, 1000), (8, 10000), (16, 1000), (16, 10000), (100, 1000), (1000, 50))){
    test(s"Like count on 1 post (parallel) ($t threads, $op users)") {
      val user = store.createUser()
      //val post = user.post(text(rand.nextInt(text.length)))
      val post = user.post("The post")

      //val semaphore: Semaphore = new Semaphore()
      val latch: CountDownLatch = new CountDownLatch(t)
      val startLatch: CountDownLatch = new CountDownLatch(1)

      val users: ListBuffer[RemoteUser] = new ListBuffer[RemoteUser]()

      for (_ <- 1 to op) {
        val testUser = store.createUser()
        users += testUser
      }

      for(_ <- 1 to t){
        val thread = new Thread {
          override def run(): Unit = {
            startLatch.await()
            for (j <- 1 to op) {
              users(j-1).like(post)
            }

            //count up semaphore
            latch.countDown()
          }
        }
        //save thread
        thread.start()
      }


      startLatch.countDown()

      //wait on semaphore
      latch.await()

      //assert
      assert(post.getLikes == op)
    }

  }

  for ((t, op) <- List( (1,100000), (2, 50000), (4, 25000), (8, 12500), (16, 6250), (24, 4167), (50, 2000), (100, 1000), (1000, 100))){
    test(s"1 post, 1 user, many operations (parallel) ($t threads, $op operations each)") {
      val user = store.createUser()
      //val post = user.post(text(rand.nextInt(text.length)))
      val post = user.post("The post")

      //val semaphore: Semaphore = new Semaphore()
      val latch: CountDownLatch = new CountDownLatch(t)
      val startLatch: CountDownLatch = new CountDownLatch(1)

      for(i <- 1 to t){
        val comment = s"thread $i, "

        val thread = new Thread {
          override def run(): Unit = {
            startLatch.await()

            user.like(post)
            for (j <- 1 to op) {
              user.like(post)
              user.comment(post, comment + j)
            }

            //count up semaphore
            latch.countDown()
          }
        }
        //save thread
        //threads += thread
        thread.start()
      }

      //for (t <- threads.toList)
      //t.start()
      startLatch.countDown()

      //wait on semaphore
      latch.await()

      //one user
      assert(post.getLikes == 1)

      val comments = post.getComments
      assert(comments.length == t*op)
      val texts = comments.map(post => post.getMessage)
      Sorting.quickSort(texts)

      for ((a, i) <- texts.zipWithIndex) {
        if(i != texts.length-1)
          assert(a != texts(i+1), "Comments should be unique here")
      }
    }
  }

  for ((t, op) <- List((5, 1000), (5, 10000), (8, 1000), (8, 10000), (16, 1000), (16, 10000), (100, 1000), (1000, 50))){

    test(s"Likes/comments on 1 post using ids (parallel) ($t threads, $op operations)") {
      val user = store.createUser()

      //using the id this time
      val post = user.post("The post")
      val pid = post.getId

      //val semaphore: Semaphore = new Semaphore()
      val latch: CountDownLatch = new CountDownLatch(t)
      val startLatch: CountDownLatch = new CountDownLatch(1)

      for (i <- 1 to t) {
        val uid = store.createUser().getId

        val comment = s"i am # $i, " //text(rand.nextInt(text.length))

        val thread = new Thread {
          override def run(): Unit = {
            startLatch.await()
            for (j <- 1 to op) {
              store.like(uid, pid)
              //testUser.like(pid)
              store.makeComment(uid, pid, comment+j)
              //testUser.comment(pid, comment + j)
            }

            //count up semaphore
            latch.countDown()
          }
        }
        //save thread
        thread.start()
      }

      startLatch.countDown()

      //wait on semaphore
      latch.await()

      assert(post.getLikes == t)
      val comments = post.getComments
      assert(comments.length == t*op)

      val texts = comments.map(post => post.getMessage)
      Sorting.quickSort(texts)

      for ((a, i) <- texts.zipWithIndex) {
        if(i != texts.length-1)
          assert(a != texts(i+1), "Comments should be unique here")
      }

    }
  }

  for ((t, op) <- List((5, 1000), (5, 10000), (8, 1000), (8, 10000), (16, 1000), (16, 10000), (100, 1000), (1000, 50))){
    test(s"Many users making posts (parallel) ($t threads, $op posts)"){

      //we use allposts(), so we have to reset the store
      store = new LocalStore("NewStore")

      val latch: CountDownLatch = new CountDownLatch(t)
      val startLatch: CountDownLatch = new CountDownLatch(1)

      for (i <- 1 to t) {
        val testUser = store.createUser()
        val message = s"i am # $i, " //text(rand.nextInt(text.length))

        val thread = new Thread {
          override def run(): Unit = {
            startLatch.await()
            for (j <- 1 to op)
              testUser.post(message + j)

            //count up semaphore
            latch.countDown()
          }
        }
        //save thread
        thread.start()
      }

      startLatch.countDown()

      latch.await()

      val posts = store.getAllPosts
      assert(posts.length == t*op)

      val texts = posts.map(post => post.getMessage)
      Sorting.quickSort(texts)

      for ((a, i) <- texts.zipWithIndex) {
        if(i != texts.length-1)
          assert(a != texts(i+1), "Messages should be unique here")
      }

    }
  }

  for ((t, op) <- List((5, 1000), (5, 10000), (8, 1000), (8, 10000), (16, 1000), (16, 10000), (100, 1000), (1000, 50))){
    test(s"Many users making, liking, and liking posts (parallel) ($t threads, $op operations)"){

      //we use allposts(), so we have to reset the store
      store = new LocalStore("NewStore")

      val latch: CountDownLatch = new CountDownLatch(t)
      val startLatch: CountDownLatch = new CountDownLatch(1)

      for (i <- 1 to t) {
        val testUser = store.createUser()
        val message = s"i am # $i, " //text(rand.nextInt(text.length))

        val thread = new Thread {
          override def run(): Unit = {
            for (j <- 1 to op)
              testUser.post(message + j)

            //count up semaphore
            latch.countDown()
          }
        }
        //save thread
        thread.start()
      }

      latch.await()

      //we now have a bunch of posts
      val posts: Array[RemotePost] = store.getAllPosts
      assert(posts.length == t*op)

      val latch2: CountDownLatch = new CountDownLatch(t)

      //if every user liked every post it would be t*op*t likes total,
      //50000000 at the most. this will take a while
      //so I'm randomly sampling op posts for each user, then counting the sum

      for(i <- 1 to t){
        //screw it, we'll make new users
        val testUser = store.createUser()
        val message = text(rand.nextInt(text.length))

        val sample = Random.shuffle(posts.toList).take(op)

        val thread = new Thread{
          override def run(): Unit = {
            startLatch.await()
            for(p <- sample) {
              testUser.comment(p, message)
              testUser.like(p)
            }
            latch2.countDown()
          }
        }
        thread.start()
      }

      startLatch.countDown()
      latch2.await()

      //lets look at the original list of posts
      assert(posts.length == t*op)

      val likes = posts.foldRight(0L)((p, a) => a + p.getLikes)
      assert(likes == t*op)

      val comments = posts.foldRight(0L)((p, a) => a + p.getComments.length)
      assert(comments == t*op)

    }
  }
}
