package ctest

import java.rmi.server.{RemoteObject, UnicastRemoteObject}

import javagram.server.{LocalStore, Server, Store}
import org.scalatest.{BeforeAndAfterAllConfigMap, ConfigMap, Suite}

trait ClientSetup extends BeforeAndAfterAllConfigMap{ self: Suite =>
  //used by sockets
  var address: String = _
  var port: Int = _

  //used by rmi
  var store: Store = _
  var localStore: LocalStore = null

  //note: in intellij you set the address using test options
  override def beforeAll(configMap: ConfigMap): Unit = {
    if(configMap.get("address").isDefined){

      val arg = configMap.get("address").get.toString//.split(":")
      address = arg.split(":")(0)
      port = arg.split(":")(1).toInt + 1

      val service: String = "rmi://" + arg + "/" + Server.SERVER_NAME

      store = java.rmi.Naming.lookup(service).asInstanceOf[Store]
    }
    else{


      localStore = new LocalStore("TestStore")
      val server: Server = new Server(localStore)
      store = RemoteObject.toStub(localStore).asInstanceOf[Store]
      address = "localhost"
      port = server.start(0) + 1
    }

  }

  override def afterAll(configMap: ConfigMap){
    if(localStore != null)
      UnicastRemoteObject.unexportObject(localStore, false)
  }

}
