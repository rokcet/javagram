package ctest

import java.io.{BufferedReader, InputStreamReader, PrintWriter}
import java.net.Socket
import java.util.concurrent.CountDownLatch

import javagram.data.RemotePost
import org.scalatest.FunSuite

import scala.collection.mutable.ListBuffer
import scala.util.Sorting

/**
  * Mostly for testing connectivity and interoperability
  * The real concurrency tests are in server tests
  *
  * There is one RMI and one socket client simulated per thread
  */
class ClientSuite extends FunSuite with ClientSetup{


  for ((t, op) <- List((2, 10), (5, 100), (20, 1000))) {
    //20 rmi, 20 sockets
    //it's pretty hard to test by looking at what sockets send back, so checking will be done with rmi
    //test("Many 20 RMI clients and 20 Socket clients") {
    test(s"$t RMI clients and $t socket clients making posts ($t threads, $op operations)"){

      //first I'll use rmi to make some clients for sockets, we need to store the ids
      val socketIds: Array[Long] = new Array[Long](t)
      val rmiIds: Array[Long] = new Array[Long](t)
      //to iterate through the posts
      val testingPosts: ListBuffer[RemotePost] = new ListBuffer[RemotePost]

      for (i <- 1 to t)
        socketIds(i-1) = store.createUser().getId

      for (i <- 1 to t)
        rmiIds(i-1) = store.createUser().getId

      val latch: CountDownLatch = new CountDownLatch(2*t)
      val startLatch: CountDownLatch = new CountDownLatch(1)

      for(id <- rmiIds){
        val rmiUser = store.getRemoteUser(id)
        val message = s"i am # ${rmiUser.getId}, rmi, " //text(rand.nextInt(text.length))

        new Thread {
          override def run(): Unit = {
            startLatch.await()
            for (j <- 1 to op)
              rmiUser.post(message + j)
            latch.countDown()
          }
        }.start()

      }

      val socket: Socket = new Socket(address, port)
      val out: PrintWriter = new PrintWriter(socket.getOutputStream, true)

      for(id <- socketIds.map(l => l.toString)){

        new Thread {
          override def run(): Unit = {
            startLatch.await()

            val responder: BufferedReader = new BufferedReader(new InputStreamReader(socket.getInputStream))

            //var line: String = _
            var response: String = ""
            val msg = s"i am # $id, socket, " //text(rand.nextInt(text.length))

            // keep reading until "over and out" is input
            for (j <- 1 to op) {
              out.println(s"post/$id/$msg" + j)

              response = responder.readLine()
              while (!response.equals("over"))
                response = responder.readLine()
            }


            // close the connection

            latch.countDown()
          }
        }.start()
      }

      startLatch.countDown()
      latch.await()

      out.println("oao")
      out.close()
      socket.close()

      //very expensive operation, careful using getposts
      val rmiPosts = rmiIds.foldLeft(List[RemotePost]())((list, id) => {
        store.getRemoteUser(id).getPosts.toList ::: list
      })

      val socketPosts = socketIds.foldLeft(List[RemotePost]())((list, id) => {
        store.getRemoteUser(id).getPosts.toList ::: list
      })

      val posts = (rmiPosts ::: socketPosts).toArray


      assert(posts.length == 2*t*op)

      val texts = posts.map(post => post.getMessage)
      Sorting.quickSort(texts)

      for ((a, i) <- texts.zipWithIndex) {
        if(i != texts.length-1)
          assert(a != texts(i+1), "Messages should be unique here")
      }

    }
  }
}

