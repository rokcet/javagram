import org.scalatest.Suites
import stest.{ConcurrentSuite, SequentialSuite, SimpleSuite}

//the tests get ran at the same time and it just doesn't work
class ServerTests extends Suites(
  new SequentialSuite,
  new ConcurrentSuite,
  new SimpleSuite
)
