package javagram.client;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.Scanner;

public class SocketClient {
  // initialize socket and input output streams

  // constructor to put ip address and port
  public SocketClient(String address, int port)
  {
    // establish a connection
    Scanner input = new Scanner(System.in);
    PrintWriter out;
    BufferedReader responder;
    Socket socket;
    try {
      socket = new Socket(address, port);
      System.out.println("Connected");

      // takes input from terminal

      // sends output to the socket
      out = new PrintWriter(socket.getOutputStream(), true);
      responder = new BufferedReader(new InputStreamReader(socket.getInputStream()));

      // string to read message from input
      String line = "";
      String response = "";

      // keep reading until "over and out" is input
      while(!line.equals("oao")) {
        line = input.nextLine();
        //line = input.nextLine();
        System.out.println(line);
        //out = new PrintWriter(socket.getOutputStream(), true);
        out.println(line);
        out.flush();

        response = responder.readLine();
        while (!response.equals("over")){

          System.out.println(response);
          response = responder.readLine();
        }

      }

      out.println(line);
      // close the connection
      input.close();
      out.close();
      socket.close();
    }
    catch(Exception i) {
      System.out.println(i);
    }
  }

  public static void main(String[] args) {
    String[] address = args[0].split(":");

    SocketClient client = new SocketClient(address[0], Integer.parseInt(address[1]));
  }
}
