package javagram.client;

import javagram.data.RemotePost;
import javagram.data.RemoteUser;
import javagram.server.Server;
import javagram.server.Store;

import java.net.MalformedURLException;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.util.Scanner;

public class Client {
  private final Store store;

  private Client(String service) throws RemoteException, NotBoundException, MalformedURLException {
    store = (Store) java.rmi.Naming.lookup(service);
  }

  private void createAccount() throws RemoteException {
    System.out.printf("user created: %d%n", store.createUser().getId());
  }

  private void closeAccount(long id) throws RemoteException {
    store.deleteUser(id);
    System.out.println("account closed");
  }

  /**
   * Very simple, just test all the functionalities in a single thread
   * Uses 2 users and 2 posts
   */
  private void simpleTest() throws RemoteException{
    RemoteUser user1 = store.createUser();
    RemoteUser user2 = store.createUser();

    RemotePost post1 = user1.post("asdf");
    //assert store.getAllPosts().length == 1;
    RemotePost post2 = user2.post("jkl;");
    //assert store.getAllPosts().length == 2;

    System.out.println(user1.summary());
    System.out.println(user2.summary());
    System.out.println(post1.header());
    System.out.println(post2.header());

    user1.like(post1);
    user2.like(post1);

    RemotePost comment1 = user1.comment(post1, "yes");
    RemotePost comment2 = user2.comment(comment1, "no");

    user1.follow(user2);

    System.out.println(user1.summary());
    System.out.println(user2.summary());
    System.out.println(post1.summary(0));
    System.out.println(post2.summary(0));
    System.out.println(comment1.header());
    System.out.println(comment2.header());
  }

  /**
   * This is a rmi cli application simulation
   * @param myId
   * @throws RemoteException
   */
  private void RMIApp() throws RemoteException {
    //RemoteUser user = store.getRemoteUser(myId);
    RemoteUser user = store.createUser();
    //System.out.println(account.feed());

    Scanner in = new Scanner(System.in); // not closed on purpose
    try {
      while (true) {
        String line = in.nextLine().trim();

        if (line.isEmpty()) {
          //System.out.println(account.feed());
          continue;
        }

        //f(ollow), l(ike), p(ost), c(omment), a(ll), m(yfeed), v(iew post), u(ser), n(ew user)
        String[] cmd = line.split("/");

        char command = cmd[0].toLowerCase().charAt(0);
        long id = -1;
        try {
          if (cmd.length > 1 && command != 'p') {
            id = Long.parseLong(cmd[1]);
          }

          switch (command) {
            case 'f':
              boolean f = user.follow(id);
              System.out.println(f ? "Followed " : "Already following " + id);
              break;
            case 'l':
              long l = user.like(id);
              System.out.println(l + " likes");
              break;
            case 'p':
              RemotePost p = user.post(cmd[1]);
              System.out.println(p.header());
              break;
            case 'c':
              RemotePost c = user.comment(id, cmd[2]);
              System.out.println(c.header());
              break;
            case 'a':
              System.out.println(store.allPosts());
              break;
            case 'm':
              System.out.println(user.feed());
              break;
            case 'v':
              System.out.println(store.getRemotePost(id).summary(0));
              break;
            case 'u':
              System.out.println(store.getRemoteUser(id).summary());
              break;
            case 'n':
              user = store.createUser();
              System.out.printf("Now as user %d\n", user.getId());
              break;
            default:
              System.out.println("Invalid command");
              break;
          }
        }
        catch (Exception e){
          System.out.println("Invalid command");
        }


      }
    } catch (java.util.NoSuchElementException e) {
      /* EOF */
    }
  }

  /**
   * Command-line client.
   *
   * The first parameter is the name of a store server (of the form {@code host:port}).
   *
   * The second parameter is to choose between rmi or sockets.
   *
   */
  public static void main(String[] args) throws Exception {
    String service = "rmi://" + args[0] + "/" + Server.SERVER_NAME;

    Client client = new Client(service);

    client.RMIApp();

  }
}
