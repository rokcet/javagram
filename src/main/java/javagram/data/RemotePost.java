package javagram.data;

import java.rmi.RemoteException;

/**
 * Represents a post
 */
public interface RemotePost extends java.rmi.Remote {
  long getId() throws RemoteException;

  /**
   * Comment on a post with a user
   */
  //RemotePost comment(RemoteUser user, String message) throws RemoteException;
  RemotePost comment(long uid, String message) throws RemoteException;

  /**
   * Like a post with a user
   */
  long like(RemoteUser user) throws RemoteException;
  long like(long uid) throws RemoteException;

  /**
   * Get a quick summary of the post
   */
  String header() throws RemoteException;

  /**
   * An indepth summary of the post, including children
   * @param level
   * @return
   * @throws RemoteException
   */
  String summary(int level) throws RemoteException;

  long getLikes() throws RemoteException;

  String getMessage() throws RemoteException;

  //Wouldn't recommend calling this except for testing
  RemotePost[] getComments() throws RemoteException;
}
