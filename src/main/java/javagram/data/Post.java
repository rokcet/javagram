package javagram.data;

import javagram.server.LocalStore;
import org.json.JSONArray;
import org.json.JSONObject;

import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
import java.util.Map;
import java.util.Queue;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.atomic.AtomicLong;

public class Post extends UnicastRemoteObject implements RemotePost {
  private final long id;
  private final String message;

  private final LocalStore store;
  private final User user;
  private final ConcurrentLinkedQueue<RemotePost> children;
  private final ConcurrentHashMap<Long, Object> likers;

  private final AtomicLong likes;

  public Post(User user, long pid, String message, LocalStore store) throws RemoteException{
    this.user = user;
    this.id = pid;
    this.message = message;
    this.store = store;

    children = new ConcurrentLinkedQueue<>();
    likers = new ConcurrentHashMap<>();
    likes = new AtomicLong();
  }

  @Override
  public long getId()  throws RemoteException{
    return id;
  }

  /**
   * Maybe just iterate thorugh the list?
   * @param level
   * @return
   * @throws RemoteException
   */
  @Override
  public String summary(int level) throws RemoteException{
    StringBuilder s = new StringBuilder();
    StringBuilder tabs = new StringBuilder();
    for(int i = 0; i < level; i++)
      tabs.append("\t");

    Queue<RemotePost> childs = new ConcurrentLinkedQueue<>(children);

    s.append(tabs);
    s.append(String.format("User %d:%s\t%d likes\t%d comments\tid:%d\n", user.getId(), message, likes.get(), childs.size(), id));

    for (RemotePost child: childs)
      s.append(tabs).append(child.summary(level + 1));

    return s.toString();
  }

  @Override
  public RemotePost comment(long uid, String message) throws RemoteException {
    RemotePost temp = store.buildComment(uid, id, message);
    children.add(temp);
    return temp;
  }

  public RemotePost comment(RemotePost comment) throws RemoteException {
    children.add(comment);
    return comment;
  }

  /**
   * Like a post with a user
   *
   * @param user
   */
  @Override
  public long like(RemoteUser user) throws RemoteException {
    return like(user.getId());
  }

  @Override
  public long like(long uid) throws RemoteException {
    Object u = likers.put(uid, new Object());
    if(u == null)
      return likes.incrementAndGet();

    return likes.get();
  }

  /**
   * Get a quick summary of the post
   */
  @Override
  public String header() throws RemoteException{
    return String.format("User %d:%s\t%d likes\t%d comments\tid:%d\n", user.getId(), message, likes.get(), children.size(), id);
  }

  /**
   * Only ran from server
   * @return
   */
  public JSONObject getJSON() throws RemoteException{
    JSONObject postObject = new JSONObject();

    postObject.put("id", id);
    postObject.put("msg", message);
    postObject.put("user", user.getId());

    //likers.keys();
    JSONArray likersArray = new JSONArray();
    for(Map.Entry<Long, Object> u: likers.entrySet())
      likersArray.put(u.getKey());
    postObject.put("likers", likersArray);

    JSONArray childrenArray = new JSONArray();
    for(RemotePost p: children)
      childrenArray.put(p.getId());
    postObject.put("children", childrenArray);

    return postObject;
  }

  @Override
  public long getLikes() throws RemoteException{
    return likes.get();
  }

  @Override
  public String getMessage() throws RemoteException{
    return message;
  }

  @Override
  public RemotePost[] getComments() throws RemoteException {
    return children.toArray(new RemotePost[0]);
  }
}
