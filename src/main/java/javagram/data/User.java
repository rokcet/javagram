package javagram.data;

import javagram.server.LocalStore;
import org.json.JSONArray;
import org.json.JSONObject;

import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentLinkedDeque;

public class User extends UnicastRemoteObject implements RemoteUser {
  private final LocalStore store;
  private final long id;
  private final Map<RemoteUser, Object> following;
  private final Deque<RemotePost> posts;

  //I like this, it's good
  public User(long id, LocalStore store) throws RemoteException{
    this.id = id;
    this.store = store;
    following = new ConcurrentHashMap<>();
    posts = new ConcurrentLinkedDeque<>();
  }

  @Override
  public long getId() {
    return id;
  }

  @Override
  public String summary(){
    StringBuilder s = new StringBuilder();
    s.append(String.format("User %d Posts:\n", id));

    //make immutable list
    Queue<RemotePost> ps = new LinkedList<>(posts);

    for (RemotePost p : ps) {
      try {
        s.append(p.header());
      } catch (RemoteException e) {
        e.printStackTrace();
      }
    }

    return s.toString();
  }

  @Override
  public String feed() throws RemoteException {
    StringBuilder s = new StringBuilder();
    s.append(String.format("User %d Feed:\n", id));

    //make a copy
    Map<RemoteUser, Object> fg = new HashMap<>(following);

    for (RemoteUser remoteUser : fg.keySet()) {
      RemotePost l = remoteUser.lastPost();
      if(l != null)
      s.append(l.header());
    }

    return s.toString();
  }

  @Override
  public RemotePost lastPost(){
    try {
      return posts.getLast();
    }
    catch(NoSuchElementException e){
      return null;
    }
  }

  @Override
  public RemotePost[] getPosts() throws RemoteException {
    return posts.toArray(new RemotePost[0]);

  }

  /**
   * Follow a user with the user
   *
   * @param user
   */
  @Override
  public boolean follow(RemoteUser user) throws RemoteException {
    return following.put(user, new Object()) == null;
  }

  /**
   * Follow a user
   *
   * @param uid
   * @throws RemoteException
   */
  @Override
  public boolean follow(long uid) throws RemoteException {
    return follow(store.getRemoteUser(uid));
  }

  /**
   * Likes the post and
   * Returns the amount of likes with the post
   *
   * @param post
   */
  @Override
  public long like(RemotePost post) throws RemoteException {
    return post.like(this);
  }

  @Override
  public long like(long pid) throws RemoteException {
    return like(store.getRemotePost(pid));
  }

  /**
   * Makes a new comment
   * Returns the new comment, which is also a post
   *
   * @param post
   * @param comment
   */
  @Override
  public RemotePost comment(RemotePost post, String comment) throws RemoteException {
    return post.comment(this.id, comment);
  }

  @Override
  public RemotePost comment(long pid, String comment) throws RemoteException {
    return comment(store.getRemotePost(pid), comment);
  }

  /**
   * Make a post
   *
   * @param message
   * @return
   * @throws RemoteException
   */
  @Override
  public RemotePost post(String message) throws RemoteException {
    RemotePost temp = store.buildPost(this.id, message);
    posts.add(temp);
    return temp;
  }

  /**
   * The post is already made, we just need to add it to the list
   * @param post
   * @return
   * @throws RemoteException
   */
  @Override
  public RemotePost post(RemotePost post) throws RemoteException {
    posts.add(post);
    return post;
  }

  /**
   * Only ran from server
   * @return
   */
  public JSONObject getJSON() throws RemoteException{
    JSONObject postObject = new JSONObject();

    postObject.put("id", id);

    JSONArray followingArray = new JSONArray();
    for(Map.Entry<RemoteUser, Object> u: following.entrySet())
      followingArray.put(u.getKey().getId());
    postObject.put("following", followingArray);

    JSONArray postedArray = new JSONArray();
    for(RemotePost p: posts)
      postedArray.put(p.getId());
    postObject.put("posted", postedArray);

    return postObject;
  }
}
