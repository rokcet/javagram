package javagram.data;

import java.rmi.RemoteException;

/**
 * A remote object sent to the client so they can do operations
 */
public interface RemoteUser extends java.rmi.Remote {
  long getId() throws RemoteException;
  /**
   * Follow a user with the user
   */
  boolean follow(RemoteUser user) throws RemoteException;
  boolean follow(long uid) throws RemoteException;

  /**
   * Likes the post and
   * Returns the amount of likes with the post
   */
  long like(RemotePost post) throws RemoteException;
  long like(long pid) throws RemoteException;

  /**
   * Makes a new comment
   * Returns the new comment, which is also a post
   */
  RemotePost comment(RemotePost post, String comment) throws RemoteException;
  RemotePost comment(long pid, String comment) throws RemoteException;

  /**
   * Make a post
   * @param message
   * @return
   * @throws RemoteException
   */
  RemotePost post(String message) throws RemoteException;

  RemotePost post(RemotePost post) throws RemoteException;

  /**
   * Used to get a users feed
   * @return
   * @throws RemoteException
   */
  String feed() throws RemoteException;

  /**
   * Used to build a feed
   * @return
   * @throws RemoteException
   */
  RemotePost lastPost() throws RemoteException;

  RemotePost[] getPosts() throws RemoteException;

  String summary() throws RemoteException;
}
