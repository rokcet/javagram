package javagram.server;

import javagram.data.Post;
import javagram.data.RemotePost;
import javagram.data.RemoteUser;
import javagram.data.User;
import javagram.exceptions.UserException;
import org.json.JSONArray;
import org.json.JSONObject;
import org.json.JSONTokener;

import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicLong;
//import org.json.JSONArray;

public class LocalStore extends UnicastRemoteObject implements Store{

  /**
   * The name of the bank, as specified in the constructor.
   */
  public final String name;
  private final File file;

  private final ConcurrentHashMap<Long, User> users;
  private final ConcurrentHashMap<Long, Post> posts;
  private AtomicLong nUsers, nPosts;

  /**
   * Builds a new store. It is immediately exported.
   *
   * @param name the name of the store
   * @throws RemoteException if the store cannot be exported
   * @see UnicastRemoteObject#UnicastRemoteObject()
   */
  public LocalStore(String name) throws RemoteException {
    this.name = name;
    file = null;
    users = new ConcurrentHashMap<>();
    posts = new ConcurrentHashMap<>();
    nUsers = new AtomicLong();
    nPosts = new AtomicLong();
  }

  public LocalStore(String name, File file) throws RemoteException, IOException {
    //this(name);
    this.file = file;
    this.name = name;
    users = new ConcurrentHashMap<>();
    posts = new ConcurrentHashMap<>();

    JSONTokener jsonTokener = new JSONTokener(new FileReader(file));
    JSONObject jsonObject = new JSONObject(jsonTokener);


    long maxUid = -1;
    long maxPid = -1;
    JSONArray userArray = jsonObject.getJSONArray("users");
    JSONArray postArray = jsonObject.getJSONArray("posts");
    //make users
    for(int i = 0; i < userArray.length(); i++) {
      JSONObject user = userArray.getJSONObject(i);

      long id = user.getLong("id");

      User u = new User(id, this);
      users.put(id, u);

      if(id > maxUid)
        maxUid = id;

    }

    for(int i = 0; i < postArray.length(); i++) {
      JSONObject post = postArray.getJSONObject(i);

      long id = post.getLong("id");
      String msg = post.getString("msg");
      long uid = post.getLong("user");

      Post p = new Post(users.get(uid), id, msg, this);
      posts.put(id, p);

      if(id > maxPid)
        maxPid = id;


      JSONArray likers = post.getJSONArray("likers");
      for(int j = 0; j < likers.length(); j++)
        users.get(likers.getLong(j)).like(p);

    }

    for(int i = 0; i < postArray.length(); i++) {
      JSONObject post = postArray.getJSONObject(i);

      long id = post.getLong("id");
      Post p = posts.get(id);

      JSONArray children = post.getJSONArray("children");
      for(int j = 0; j < children.length(); j++)
        p.comment(posts.get(children.getLong(j)));

    }

    for(int i = 0; i < userArray.length(); i++) {
      JSONObject user = userArray.getJSONObject(i);

      long id = user.getLong("id");
      User u = users.get(id);

      JSONArray following = user.getJSONArray("following");
      for(int j = 0; j < following.length(); j++)
        u.follow(following.getLong(j));

      JSONArray postedArray = user.getJSONArray("posted");
      for(int j = 0; j < postedArray.length(); j++)
        u.post(posts.get(postedArray.getLong(j)));
    }

    nUsers = new AtomicLong(maxUid);
    nPosts = new AtomicLong(maxPid);
  }

  @Override
  public String toString() {
    return "LocalStore: " + name;
  }

  public long[] getAllUsers(){
    Long[] result = users.keySet().toArray(new Long[0]);
    Arrays.sort(result);
    long[] res = new long[result.length];
    for(int i = 0; i < res.length; i++)
      res[i] = result[i];
    return res;
  }

  /**
   * Get ids of all the posts
   *
   * @return
   * @throws RemoteException
   */
  @Override
  public RemotePost[] getAllPosts() throws RemoteException {
    return new ArrayList<RemotePost>(posts.values()).toArray(new RemotePost[0]);
  }

  @Override
  public RemoteUser createUser() throws RemoteException {
    long uid = nUsers.incrementAndGet();
    User temp = new User(uid, this);
    users.put(uid, temp);
    return temp;
  }

  /**
   * This is used by users
   *
   * @param uid
   * @param message
   * @return
   * @throws RemoteException
   */
  //@Override
  public RemotePost buildPost(long uid, String message) throws RemoteException {
    long pid = nPosts.incrementAndGet();
    Post temp = new Post(users.get(uid), pid, message, this);
    posts.put(pid, temp);
    return temp;
  }

  /**
   * This is used by commands, and goes to the users,
   * @param uid
   * @param message
   * @return
   * @throws RemoteException
   */
  @Override
  public RemotePost makePost(long uid, String message) throws RemoteException{
    RemotePost temp = buildPost(uid, message);
    users.get(uid).post(temp);

    return temp;
  }

  @Override
  public RemotePost buildComment(long uid, long parent, String message) throws RemoteException {
    long pid = nPosts.incrementAndGet();
    Post temp = new Post(users.get(uid), pid, message, this);
    posts.put(pid, temp);
    return temp;
  }

  @Override
  public RemotePost makeComment(long uid, long parent, String message) throws RemoteException {
    RemotePost comment = buildComment(uid, parent, message);
    posts.get(parent).comment(comment);
    return comment;
  }

  @Override
  public boolean follow(long uid1, long uid2) throws RemoteException {
    return users.get(uid1).follow(users.get(uid2));
  }

  @Override
  public long like(long uid, long pid) throws RemoteException {
    return posts.get(pid).like(users.get(uid));
  }

  @Override
  public RemoteUser getRemoteUser(long id) throws RemoteException {
    RemoteUser r = users.get(id);
    if(r == null)
      throw new UserException("No account found");

    return r;
  }

  @Override
  public RemotePost getRemotePost(long pid) throws RemoteException {
    RemotePost r = posts.get(pid);
    if(r == null)
      throw new UserException("No account found");

    return r;
  }

  @Override
  public void deleteUser(long id) throws RemoteException {
    if(!users.containsKey(id))
      throw new UserException("No account found");

    //RemoteUser deleted = users.get(id);
    users.remove(id);
    //((User)deleted).close();
  }

  /**
   * Probably for sockets
   * @param command
   * @return
   * @throws RemoteException
   */
  public String runCommand(String command) throws RemoteException {
    String[] args = command.split("/");
    String op = args[0];
    long id = -1;
    try {
      if(args.length > 1) {
        id = Long.parseLong(args[1]);

        if (!users.containsKey(id))
          return "user not found";
      }

      //return "bruh";
      String s;
      switch (op) {
        case "follow":
          follow(id, Long.parseLong(args[2]));
          return id + " following " + args[2];
        case "like":
          return Long.toString(like(id, Long.parseLong(args[2])));
        case "post":
          return makePost(id, args[2]).header();
        case "comment":
          return makeComment(id, Long.parseLong(args[2]), args[3]).header();
        case "all":
          return allPosts();
        case "myfeed":
          return getRemoteUser(id).feed();
        case "viewpost":
          return getRemotePost(id).summary(0);
        case "user":
          return getRemoteUser(id).summary();
        case "new":
          createUser();
          return "complete";

      }
    }
    catch(Exception e){
      return e.getMessage();
    }
    return "invalid command";

  }

  /**
   * A snapshot of all the posts.  It is only an approximation.
   * The returned map is not a live view and will not reflect future posts
   *
   * @return a summary of all posts
   */
  public String allPosts(){
    //clone the thing
    Map<Long, Post> copy = Collections.unmodifiableMap(new HashMap<>(posts));
    StringBuilder result = new StringBuilder();

    for(Map.Entry<Long, Post> entry: copy.entrySet()) {
      try {
        result.append(entry.getValue().header());
      }
      catch(RemoteException e){}
    }

    return result.toString();
  }

  public void saveToFile() throws RemoteException, IOException{
    FileWriter out;
    if(file == null)
      out = new FileWriter(new File("out.json"));
    else
      out = new FileWriter(file);

    JSONArray userArray = new JSONArray();
    JSONArray postArray = new JSONArray();

    //for(Post p:  posts)
    for(Map.Entry<Long, Post> p: posts.entrySet())
      postArray.put(p.getValue().getJSON());

    for(Map.Entry<Long, User> u: users.entrySet())
      userArray.put(u.getValue().getJSON());

    JSONObject jsonObject = new JSONObject();

    jsonObject.put("users", userArray);
    jsonObject.put("posts", postArray);

    out.write(jsonObject.toString());
    out.flush();

    out.close();

  }
}
