package javagram.server;

import javagram.data.RemotePost;
import javagram.data.RemoteUser;
import javagram.data.User;
import javagram.exceptions.UserException;

import java.rmi.Remote;
import java.rmi.RemoteException;

public interface Store extends java.rmi.Remote{

  /**
   * All users.  This may only be an approximation if accounts are added/removed while this call
   * is taking place.
   *
   * @return a newly allocated array of uid
   */
  long[] getAllUsers() throws RemoteException;

  /**
   * Get ids of all the posts
   * @return
   * @throws RemoteException
   */
  RemotePost[] getAllPosts() throws RemoteException;

  String allPosts() throws RemoteException;

  /**
   * A remote account.
   *
   * @return a remote account (stub) backed by the corresponding account in the bank
   * @throws UserException if the uid does not correspond to a user
   */
  RemoteUser getRemoteUser(long uid) throws RemoteException;

  /**
   * The remote post
   *
   * @param pid
   * @return The post
   * @throws RemoteException
   */
  RemotePost getRemotePost(long pid) throws RemoteException;


  /**
   * Creates a new user
   *
   * @return a unique id of the new user
   */
  RemoteUser createUser() throws RemoteException;

  /**
   * Make a new post
   *
   * @return
   * @throws RemoteException
   */
  RemotePost makePost(long uid, String message) throws RemoteException;

  RemotePost buildComment(long uid, long parent, String message) throws RemoteException;

  RemotePost makeComment(long uid, long parent, String message) throws RemoteException;

  boolean follow(long uid1, long uid2) throws RemoteException;

  long like(long uid, long pid) throws RemoteException;

  /**
   * Closes an account.
   *
   * @throws UserException if the uid does not correspond to a user
   */
  void deleteUser(long uid) throws RemoteException;

  /**
   * Run a command, this is used by sockets
   *
   * @throws UserException if the uid does not correspond to a user
   */
  String runCommand(String command) throws RemoteException;
}
