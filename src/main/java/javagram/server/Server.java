package javagram.server;

import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;
import java.util.Random;
import java.util.Scanner;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.logging.Logger;

import static java.util.concurrent.TimeUnit.MINUTES;

public class Server {

  public static final String SERVER_NAME = "javagram_server";
  private static ScheduledExecutorService exec = Executors.newScheduledThreadPool(5);

  private final LocalStore localStore;
  private Registry registry;

  /**
   * Creates a server for the given bank.
   */
  public Server(LocalStore localStore) {
    this.localStore = localStore;
  }

  /**
   * Starts the server by binding it to a registry.
   *
   * <ul>
   *
   * <li>If {@code port} is positive, the server attempts to locate a registry at this port.</li>
   *
   * <li>If {@code port} is negative, the server attempts to start a new registry at this
   * port.</li>
   *
   * <li>If {@code port} is 0, the server attempts to start a new registry at a randomly chosen
   * port.</li>
   *
   * </ul>
   *
   * @return the registry port
   */
  public synchronized int start(int port) throws RemoteException {
    Registry reg;
    if (port > 0) { // registry already exists
      reg = LocateRegistry.getRegistry(port);
    } else if (port < 0) { // create on given port
      port = -port;
      reg = LocateRegistry.createRegistry(port);
    } else { // create registry on random port
      Random rand = new Random();
      int tries = 0;
      while (true) {
        port = 50000 + rand.nextInt(10000);
        try {
          reg = LocateRegistry.createRegistry(port);
          break;
        } catch (RemoteException e) {
          if (++tries < 10 && e.getCause() instanceof java.net.BindException)
            continue;
          throw e;
        }
      }
    }
    reg.rebind(localStore.name, localStore);
    registry = reg;

    try {
      ServerSocket serverSocket = new ServerSocket(port+1);
      System.out.printf("Sockets running on port %d\n", port+1);
      exec.submit(() -> handleSocket(serverSocket));
      exec.submit(this::handleCLI);
    }
    catch(Exception e){
      e.printStackTrace();
    }


    return port;
  }

  private void handleCLI(){
    try{
      Scanner input = new Scanner(System.in);

      String line = input.nextLine();
      while(!line.equals("done\n") && !line.equals("done")){
        System.out.println(localStore.runCommand(line));
        line = input.nextLine();
      }
      input.close();
      stop();
    }
    catch (Exception e){
      e.printStackTrace();
    }

  }

  private void handleSocket(ServerSocket serverSocket){
    while(true){
      try{
        Socket socket = serverSocket.accept();

        exec.submit(() -> {
          try{
            PrintWriter out = new PrintWriter(socket.getOutputStream(), true);
            BufferedReader in = new BufferedReader(new InputStreamReader(socket.getInputStream()));

            String input = "";

            input = in.readLine();
            while(!input.equals("oao")){

              out.print(localStore.runCommand(input) + "\nover\n");
              out.flush();

              input = in.readLine();
            }


            //out.close();
            //in.close();
            //socket.close();
          }
          catch (IOException e){
            e.printStackTrace();
          }
        });
      }
      catch (IOException e){
        e.printStackTrace();
      }
    }
  }

  /**
   * Stops the server by removing the data store from  the registry. The datastore is left exported.
   */
  public synchronized void stop() {
    exec.shutdown();
    try {
        localStore.saveToFile();
    }
    catch (Exception e){
      e.printStackTrace();
    }

    if (registry != null) {
      try {
        registry.unbind(localStore.name);
      } catch (Exception e) {
        Logger.getLogger("cs735_835").warning(String.format("unable to stop: %s%n", e.getMessage()));
      } finally {
        registry = null;
      }
    }
  }

  /**
   * Prints a feed of all the posts
   */
  public synchronized void printStatus() {
    System.out.printf("%nAll Posts:\n");
    System.out.println(localStore.allPosts());
  }

  /**
   * Command-line program.  Single (optional) argument is a port number (see {@link #start(int)}).
   */
  public static void main(String[] args) throws Exception {
    int port = 0;
    //if (args.length > 0)
      //port = Integer.parseInt(args[0]);

    LocalStore localStore;
    if (args.length > 0)
      localStore = new LocalStore(SERVER_NAME, new File(args[0]));
    else
      localStore = new LocalStore(SERVER_NAME);

    Server server = new Server(localStore);
    try {
      exec = Executors.newScheduledThreadPool(5);
      port = server.start(port);
      System.out.printf("Server running on port %d%n", port);
      exec.scheduleAtFixedRate(server::printStatus, 1, 1, MINUTES);
    } catch (RemoteException e) {
      Throwable t = e.getCause();
      if (t instanceof java.net.ConnectException)
        System.err.println("unable to connect to registry: " + t.getMessage());
      else if (t instanceof java.net.BindException)
        System.err.println("cannot start registry: " + t.getMessage());
      else
        System.err.println("cannot start server: " + e.getMessage());
      UnicastRemoteObject.unexportObject(localStore, false);
    }
    Runtime.getRuntime().addShutdownHook(new Thread(server::stop));
  }


}
