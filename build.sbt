val UNH_CS_Repository = "UNH/CS repository" at "http://cs.unh.edu/~charpov/lib"
val scalaTest = "org.scalatest" %% "scalatest" % "3.0.5"
val jcip = "net.jcip" % "jcip-annotations" % "1.0"
val apacheLang = "org.apache.commons" % "commons-lang3" % "3.8.1"
val UNH_CS = "edu.unh.cs" % "classutils" % "1.3.3"

lazy val root = (project in file(".")).
  settings(
    name := "javagram",
    version := "1.1.0",
    scalaVersion := "2.12.8",

    resolvers += UNH_CS_Repository,

    libraryDependencies ++= Seq(
      scalaTest % Test,
      UNH_CS % Test,
      jcip,
      apacheLang,
      "org.json" % "json" % "20180813"
    ),

    crossPaths := false,

    Test / logBuffered := false,
    Test / fork := true,
    Test / parallelExecution := false,
    Test / javaOptions += "-Xmx16G",
    parallelExecution in Test := false,
    
    Compile / compile / javacOptions ++= Seq("-deprecation", "-Xlint"),
    Compile / run / fork := true,
    Compile / run / javaOptions += "-Dsun.rmi.dgc.server.gcInterval=30000",

    scalacOptions ++= Seq(
      "-deprecation", // Emit warning and location for usages of deprecated APIs.
      "-encoding", "utf-8", // Specify character encoding used by source files.
      "-explaintypes", // Explain type errors in more detail.
      "-feature", // Emit warning and location for usages of features that should be imported explicitly.
      "-unchecked", // Enable additional warnings where generated code depends on assumptions.
      "-Xfuture" // Turn on future language features.
    )

  )
